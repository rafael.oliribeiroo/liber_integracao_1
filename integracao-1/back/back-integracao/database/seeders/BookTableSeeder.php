<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\Book;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $books = [
            [
            'name' => 'Código Limpo',
            'author' => 'Robert C. Martin',
            'genre' => 'informação',
            'price' => 55.00,
            'summary' => 'O livro sobre código limpo do Nathan Andrade.',
            'condition' => 'novo',
            'rating_sum' => 40.0,
            'amount_sold' => 35,
            'amount_rates' => 10,
            'avg_rating' => 4.0,
            ],
            [
                'name' => 'Segredos do JS',
                'author' => 'John Resig',
                'genre' => 'informação',
                'price' => 102.30,
                'summary' => 'O livro sobre os segredos do Nathan Andrade.',
                'condition' => 'semi-novo',
                'rating_sum' => 50.0,
                'amount_sold' => 100,
                'amount_rates' => 10,
                'avg_rating' => 5.0,
            ],
            [
                'name' => 'Teoria dos Algoritimos',
                'author' => 'Thomas H. Cormen',
                'genre' => 'informação',
                'price' => 7.00,
                'summary' => 'O livro sobre algoritmos do Nathan Andrade.',
                'condition' => 'surrado',
                'rating_sum' => 30.0,
                'amount_sold' => 27,
                'amount_rates' => 10,
                'avg_rating' => 3.0,
            ],
            [
                'name' => 'Garoto mágico',
                'author' => 'Robert C. Martin',
                'genre' => 'fantasia',
                'price' => 25.00,
                'summary' => 'O livro sobre contos do Nathan Andrade.',
                'condition' => 'novo',
                'rating_sum' => 20.0,
                'amount_sold' => 12,
                'amount_rates' => 10,
                'avg_rating' => 2.0,
                ],
                [
                    'name' => 'Dois gnomos',
                    'author' => 'John Resig',
                    'genre' => 'fantasia',
                    'price' => 12.30,
                    'summary' => 'O livro sobre as descobertas do Nathan Andrade.',
                    'condition' => 'semi-novo',
                    'rating_sum' => 10.0,
                    'amount_sold' => 77,
                    'amount_rates' => 10,
                    'avg_rating' => 1.0,
                ],
                [
                    'name' => 'Brinquedo Mágico',
                    'author' => 'Thomas H. Cormen',
                    'genre' => 'fantasia',
                    'price' => 15.50,
                    'summary' => 'O livro sobre os bonecos do Nathan Andrade.',
                    'condition' => 'surrado',
                    'rating_sum' => 47.0,
                    'amount_sold' => 3,
                    'amount_rates' => 10,
                    'avg_rating' => 4.7,
                ],
            
        ];
        foreach($books as $book){
            Book::create($book);
        }
    }
}
